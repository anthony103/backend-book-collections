from django.db import models
from django_extensions.db.models import TimeStampedModel
from django.core.validators import MaxValueValidator


class Publisher(TimeStampedModel):
    """Publisher Modal defines the pulication name and registration id"""
    name = models.CharField(max_length=128)
    registration_number = models.CharField(max_length=25)

    def __str__(self):
        return '{}'.format(self.name)


class Author(TimeStampedModel):
    """Auther may have multiple books"""
    name = models.CharField(max_length=128)
    description = models.TextField(null=True, blank=True)
    registration_number = models.CharField(max_length=25)

    def __str__(self):
        return '{}'.format(self.name)


class Book(TimeStampedModel):
    """For a publication there may be multiple books
        and Also Book there may be multiple Auther
    """
    class BOOK_CATEGORY:
        ART = 0
        MUSIC = 1
        BIOGRAPHY = 2
        COMICS = 3
        TECH = 4
        COOKING = 5

    BOOK_CATEGORY_CHOICES = (
        (BOOK_CATEGORY.ART, 'Art'),
        (BOOK_CATEGORY.MUSIC, 'Music'),
        (BOOK_CATEGORY.BIOGRAPHY, 'Biography'),
        (BOOK_CATEGORY.COMICS, 'Comics'),
        (BOOK_CATEGORY.TECH, 'Technology'),
        (BOOK_CATEGORY.COOKING, 'Cooking'),
    )

    title = models.CharField(max_length=52, verbose_name="Title", unique=True)
    tag_line = models.CharField(max_length=52, verbose_name="Book tag line", null=True, blank=True)
    isbn = models.CharField(max_length=52, verbose_name="ISBN number")
    published_at = models.PositiveIntegerField(validators=[MaxValueValidator(9999)])
    category = models.IntegerField(verbose_name='Category', choices=BOOK_CATEGORY_CHOICES)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    overview = models.TextField()

    author = models.ManyToManyField(Author, related_name="books", blank=True)
    publisher = models.ForeignKey(Publisher, related_name="books", null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return '{}'.format(self.title)

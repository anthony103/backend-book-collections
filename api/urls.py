from rest_framework.routers import DefaultRouter
from django.urls import path, include
from api.views import book_views as views

router = DefaultRouter()
router.register(r'books', views.BookViewSet, basename='book')
router.register(r'author', views.AutherViewSet, basename='author')
router.register(r'publisher', views.PublisherViewSet, basename='publisher')

urlpatterns = [
    path('v1/', include(router.urls)),
]
